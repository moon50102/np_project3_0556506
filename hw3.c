#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#define MAXUSER 5
#define EXIT_STR "exit\r\n"
#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

int write_enable[MAXUSER];
char* test_dir = "/net/gcs/105/0556506/public_html/";
char h[MAXUSER][100];
char p[MAXUSER][100];
char f[MAXUSER][100];

int contain_prompt ( char* line );

void print_replace(char* s)
{
	for(int i=0;i<strlen(s);++i)
	{

		if(s[i] == '\"') printf("&quot;");
		else if(s[i] == '\'') printf("&apos;");
		else if(s[i] == '&') printf("&amp;");
		else if(s[i] == '<') printf("&lt;");
		else if(s[i] == '>') printf("&gt;");
		else if(s[i] == '\n') printf("<br>");
		else printf("%c",s[i]);
	}
}
void print_html(char* s,int pos,int is_cmd)
{
	char temp[3000];
	strcpy(temp,s);
	for(int i=0;i<strlen(temp);++i)
	{
		if(temp[i] == '\r')
			temp[i] = ' ';
	}

	if(is_cmd == 1)
	{
		printf("<script>document.all['m%d'].innerHTML += \" <b>",pos);
		print_replace(temp);
		printf("</b><br>\";</script>\r\n");
	}
	else if(contain_prompt(s))
		printf("<script>document.all['m%d'].innerHTML += \" %s\";</script>\r\n",pos,temp);
	else
	{
		printf("<script>document.all['m%d'].innerHTML += \" ",pos);
		print_replace(temp);
		printf("<br>\";</script>\r\n");
	}
		
}

void print_default_html(int c)
{
	printf(
	"Content-Type: text/html\r\n\r\n"
	"<html>\r\n"
	"<head>\r\n"
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\"/>\r\n"
		"<title>Network Programming Homework 3</title>\r\n"
		"</head>\n"
		"<body bgcolor=#336699>\r\n"
			"<font face=\"Courier New\" size=2 color=#FFFF99>\r\n"
			"<table width=\"800\" border=\"1\">\r\n"
			"<tr>\n");
	for(int i=0;i<c;++i)
	{
		printf("<td>%s</td>",h[i]);
	}
	printf("</tr>\r\n<tr>\r\n");

	for(int i=0;i<c;++i)
	{
		printf("<td valign=\"top\" id=\"m%d\"></td>",i);
	}
	printf(
			"</tr>\r\n"
			"</table>\r\n"
			"</font>\r\n"
			"</body>\r\n"
			"</html>\r\n"
		);
}
int contain_prompt ( char* line )
{
	int i, prompt = 0 ;
	for (i=0; line[i]; ++i) {
		switch ( line[i] ) {
			case '%' : prompt = 1 ; break;
			case ' ' : if ( prompt ) return 1;
			default: prompt = 0;
		}
	}
	return 0;
} 

int recv_msg(int userno,int from)
{
	char buf[3000],*tmp;
	int len,i;
	
	len=read(from,buf,sizeof(buf)-1);
	if(len < 0) return -1;

	buf[len] = 0;
	if(len>0)
	{
		for(tmp=strtok(buf,"\n"); tmp; tmp=strtok(NULL,"\n"))
		{
			if ( contain_prompt(tmp) ) write_enable[userno] = 1 ;
			print_html(tmp,userno,0);
		}
	}
	fflush(stdout); 
	return len;
}

int readline(int fd,char *ptr,int maxlen)
{
	int n, rc;
	char c;
	*ptr = 0;
	for(n=1; n<maxlen; n++)
	{
		rc=read(fd,&c,1);
		if(rc== 1)
		{
			*ptr++ = c;
			if(c=='\n')	break;
		}
		else if(rc==0)
		{
			if(n==1)		 return 0;
			else				 break;
		}
		else return(-1);
	}
	return n;
}	


int main(void)
{
	char *data;		
	//setenv("QUERY_STRING","h1=nplinux3.cs.nctu.edu.tw&p1=8710&f1=t1.txt&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5=",1);
	data = getenv("QUERY_STRING");
	if(data == NULL)
		printf("<P>Error! Error in passing data from form to script.\n");
	else
	{
		char temp[1000];
		strcpy(temp,data);
		for(int i=0;i<strlen(temp);++i)
		{
			if(temp[i] == '&')
				temp[i] = ' ';
		}
		sscanf(temp,"h1=%s p1=%s f1=%s h2=%s p2=%s f2=%s h3=%s p3=%s f3=%s h4=%s p4=%s f4=%s h5=%s p5=%s f5=%s",&h[0],&p[0],&f[0],&h[1],&p[1],&f[1],&h[2],&p[2],&f[2],&h[3],&p[3],&f[3],&h[4],&p[4],&f[4]);
	}


	//DEBUG
	//strcpy(h[0],"nplinux3.cs.nctu.edu.tw");
	//strcpy(p[0],"8710");
	//strcpy(f[0],"t1.txt");

	
	char buf[3000], msg_buf[3000], msg_buf1[3000];
	int len, i, SERVER_PORT;
	int user_count = 0;
	int client_fd[MAXUSER];
	int unsend[MAXUSER];
	struct sockaddr_in client_sin[MAXUSER];
	struct hostent *he;
	FILE *fd[MAXUSER]; 

	fd_set rfds; /* readable file descriptors*/
	fd_set wfds; /* writable file descriptors*/
	fd_set rs; /* active file descriptors*/
	fd_set ws; /* active file descriptors*/

	int nfds = getdtablesize();
	FD_ZERO(&rfds); FD_ZERO(&wfds); FD_ZERO(&rs); FD_ZERO(&ws);
	
	

	for(i=0;i<MAXUSER;++i)
	{
		if(strlen(h[i]) > 3)
		{
			user_count++;
		}
		else continue;
		char test_path[5000];
		strcpy(test_path,test_dir);
		strcat(test_path,f[i]);
		
		fd[i] = fopen(test_path, "r");
		FD_SET(fileno(fd[i]),&rfds);

		he = gethostbyname(h[i]);
		SERVER_PORT = atoi(p[i]);

		client_fd[i] = socket(AF_INET,SOCK_STREAM,0);

		FD_SET(client_fd[i], &rs);
		FD_SET(client_fd[i], &ws);

		memset(&client_sin[i], 0, sizeof(client_sin[i])); 
		client_sin[i].sin_family = AF_INET;
		
		client_sin[i].sin_addr = *((struct in_addr *)he->h_addr);

		client_sin[i].sin_port = htons(SERVER_PORT);
	}
	

	print_default_html(user_count);

	for(i=0;i<MAXUSER;++i) unsend[i] = 0;

	memset (write_enable, 0, sizeof(write_enable));

	for(i=0;i<user_count;++i)
	{
		if(connect(client_fd[i],(struct sockaddr *)&client_sin[i],sizeof(client_sin[i])) == -1) {
			printf("connect fail\n");
			if (errno != EINPROGRESS) return (-1);	
		}
		else
		{
			fcntl(client_fd[i], F_SETFL, fcntl(client_fd[i], F_GETFL, 0) | O_NONBLOCK);
			
		}
	}
	rfds = rs; wfds = ws;
	

	int con = user_count;
	int status[MAXUSER];

	for(int i=0;i<user_count;++i) status[i] = F_CONNECTING;

	while(con > 0)
	{

		memcpy(&rfds, &rs, sizeof(rfds)); memcpy(&wfds, &ws,sizeof(wfds));
		if ( select(nfds, &rfds, &wfds, (fd_set*)0, (struct timeval*)0) < 0 ) printf("SELECT FAIL\n");

		for(i=0;i<user_count;++i)
		{
			if(status[i] == F_CONNECTING && (FD_ISSET(client_fd[i], &rfds) || FD_ISSET(client_fd[i], &wfds)))
			{
				int error; 
				int n; 
				if (getsockopt(client_fd[i], SOL_SOCKET, SO_ERROR, &error, &n) < 0 ||	error != 0)
				{
					printf("non-blocking fail");
					return (-1);
				}

				status[i] = F_READING;
				FD_CLR(client_fd[i], &ws);
			}
			else if(status[i] == F_WRITING  && FD_ISSET(client_fd[i], &wfds))
			{
				

				if(!unsend[i]) {
					//送meesage
					bzero(msg_buf,sizeof(msg_buf));
					len = readline(fileno(fd[i]), msg_buf, sizeof(msg_buf));

					if(len < 0) exit(1);
					msg_buf[len] = 0;
					fflush(stdout);
				}

				unsend[i] = 0;
				if(!strncmp(msg_buf, "exit",4))	// exit all
				{
					print_html(msg_buf,i,1);

					if(write(client_fd[i],EXIT_STR ,6) == -1) return -1;

					while(recv_msg(i, client_fd[i]) >0);
					
					FD_CLR(client_fd[i], &ws);
					status[i] = F_DONE;
					close(client_fd[i]);
					
					//client_fd[i] = socket(AF_INET,SOCK_STREAM,0);
					
					fclose(fd[i]);
					con--;
					if(con < 1) break;
				}	
				else	// send command
				{
					if ( write_enable[i] ) {
						print_html(msg_buf,i,1);
						if(i<user_count){								
							write(client_fd[i], msg_buf, strlen(msg_buf));
							write_enable[i] = 0; 
							FD_CLR(client_fd[i], &ws);
							status[i] = F_READING;
							FD_SET(client_fd[i], &rs);
						}
					} else {
						unsend[i] = 1 ;
					}
				}
			}
			else if (status[i] == F_READING && FD_ISSET(client_fd[i], &rfds) )
			{

				if(recv_msg(i, client_fd[i]) < 0)
				{
					close(client_fd[i]);
					exit(1);
				}
				
				if ( write_enable[i] )
				{
					status[i] = F_WRITING;
					FD_CLR(client_fd[i], &rs);
					FD_SET(client_fd[i], &ws);
				}

			}	
		}
		

	}
	return 0;
}