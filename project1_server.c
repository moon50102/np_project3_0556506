/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <sys/wait.h>
#include <ctype.h>

char* WELCOME_MSG = "****************************************\n** Welcome to the information server. **\n****************************************\n";
char exec_list[80][255] = {};
char cmd[5000][256];
char env_name[100][500];
char env_argv[100][5000];
int env_argc = 0;
int cmd_cnt = 0;
int exec_list_cnt = 0;
int number_pipe_fd[3000];
int number_pipe_line[3000];
int number_pipe_cnt = 0;
int number_pipe_type_array[3000];

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void parse_cmd(char* cmd_input)
{
	
	cmd_cnt = 0;

	char cmd_line[15000];

	strcpy(cmd_line,cmd_input);
	for(int i=0;i<15000;++i)
	{
		if(cmd_line[i] == '\n' || cmd_line[i] == '\r')
			cmd_line[i] = ' ';
	}

	char* token = strtok(cmd_line," ");
	for(int i=0;i<2000;i++)
		strcpy(cmd[i],"");

	while(token != NULL)
	{
		if(strcmp(token,"\n") == 0 || strcmp(token,"\r") == 0)
		{
			token = strtok(NULL," ");
			continue;
		}

		if(strncmp(token,"|",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],"|");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,"!",1) == 0 && strlen(token) > 1)
		{

			strcpy(cmd[cmd_cnt],"!");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else
		{
			strcpy(cmd[cmd_cnt],token);
			cmd_cnt = cmd_cnt + 1;
		}
		
		token = strtok(NULL," ");
	}

}


void scan_bin()
{
	DIR *dir;
	struct dirent *ent;
	char scan_path[10000] = "/net/gcs/105/0556506/ras/";
	char temp[1000];
	strcpy(temp,env_argv[0]);
	char* path_token = strtok(temp,":");
	exec_list_cnt = 0;

	while(path_token != NULL)
	{
		strcpy(scan_path,path_token);
		if ((dir = opendir (scan_path)) != NULL) 
		{
			/* print all the files and directories within directory */
			while ((ent = readdir (dir)) != NULL) 
			{
				if(strncmp(ent->d_name,".",1) != 0 && strncmp(ent->d_name,"..",2) != 0)
				{
					strcpy(exec_list[exec_list_cnt],ent->d_name);
					++exec_list_cnt;
				}
			}
			closedir(dir);
		} 
		else
		{
			/* could not open directory */
			perror ("");
		}
		path_token = strtok(NULL,":");
	}
	
}

int check_exist(char* check_cmd)
{
	scan_bin();
	for(int i=0;i<exec_list_cnt;i++)
	{
		if(strcmp(check_cmd,exec_list[i]) == 0)
			return 1;
	}
	return 0;
}

int main(int argc, char *argv[])
{
	chdir("/net/gcs/105/0556506/ras");

	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[15000];
	struct sockaddr_in serv_addr, cli_addr;
	int n;

	if (argc < 2) {
	 fprintf(stderr,"ERROR, no port provided\n");
	 exit(1);
	}

	strcpy(env_name[0],"PATH");
	strcpy(env_argv[0],"bin:."); 
	env_argc = 1;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	int enable = 1;
	if (sockfd < 0 || setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int))<0) 
		error("ERROR opening socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	    error("ERROR on binding");

	while(1)
	{
		
		
		listen(sockfd,5);
		clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

		if (newsockfd < 0) error("ERROR on accept");

		printf("new socket accepted\n");
		
		n = write(newsockfd,WELCOME_MSG,strlen(WELCOME_MSG));
		if (n < 0) error("ERROR writing to socket");

		while(1)
		{
			n = write(newsockfd,"% ",2);
			if (n < 0) error("ERROR writing to socket");

			bzero(buffer,15000);
			n = read(newsockfd,buffer,15000);
			
			if (n < 0) error("ERROR reading from socket");

			parse_cmd(buffer);

			if(strncmp(cmd[0],"exit",4) == 0)
			{
				number_pipe_cnt = 0;
				close(newsockfd);
				break;
			}
			else if(strncmp(cmd[0],"setenv",6) == 0)
			{
				int env_exist = 0;
				for(int i=0;i<env_argc;++i)
				{
					if(strcmp(env_name[i],cmd[1]) == 0)
					{
						strcpy(env_argv[i],cmd[2]);
						env_exist = 1;
						break;
					}
				}

				if(env_exist == 0)
				{
					strcpy(env_name[env_argc],cmd[1]);
					strcpy(env_argv[env_argc],cmd[2]);
					env_argc++;
				}
				
				continue;		
			}
			else if(strncmp(cmd[0],"printenv",8) == 0)
			{				
				
				for(int i=0;i<env_argc;++i)
				{
					if(strcmp(env_name[i],cmd[1]) == 0)
					{
						char temp[10000];
						strcpy(temp,"");
						strcat(temp,env_name[i]);
						strcat(temp,"=");
						strcat(temp,env_argv[i]);
						strcat(temp,"\n");

						n = write(newsockfd,temp,strlen(temp));
						if (n < 0) error("ERROR writing to socket");

						break;
					}
				}

				continue;
			}
			
			
			int pipe_number = 1;
			

			for(int i=0;i<cmd_cnt;++i)
			{
				if(strncmp(cmd[i],"|",1) == 0 || strncmp(cmd[i],"!",1) == 0)
					++pipe_number;
			}
	        
	        int write_file = 0;
	        char file_name[10000] ="";

	        for(int i=0;i<cmd_cnt;++i)
	        {
	            if(strncmp(cmd[i],">",1) == 0)
	            {
	                write_file = 1;
	                strcpy(file_name,cmd[i+1]);
	                strcpy(cmd[i+1],"") ;
	                strcpy(cmd[i],"");
	                cmd_cnt = cmd_cnt - 2;
	            }    
	        }
	     
	     	for(int i=0;i<number_pipe_cnt;++i)
	     	{
	  			if(number_pipe_line[i] > -1)
	     			number_pipe_line[i] =  number_pipe_line[i] - 1;
	     	}
	        


	        int number_pipe_out_cnt = 0;
	        int np_out[2];
	        pipe(np_out);
	        
	        if(number_pipe_cnt > 0)
	        {
	        	for(int i=0;i<number_pipe_cnt;++i)
				{

					if(number_pipe_line[i] == 0)
					{
						bzero(buffer,15000);
						if(read(number_pipe_fd[i],buffer,sizeof(buffer)) < 0)
							printf("Number Pipe read error\n");

						if(write(np_out[1],buffer,strlen(buffer)) < 0)
							printf("Number Pipe write error\n");

						

						close(number_pipe_fd[i]);
						number_pipe_out_cnt++;
					}
				}
	        }
	        if(number_pipe_out_cnt == 0)
	        {
	        	close(np_out[0]);
	        }
	     	
	     	close(np_out[1]);
	        
			int number_pipe = 1;
			int number_pipe_type = 0;

			
	        if(strncmp(cmd[cmd_cnt-2],"|",1) == 0 || strncmp(cmd[cmd_cnt-2],"!",1) == 0)
	        {

	            for(int i=0;i<strlen(cmd[cmd_cnt-1]);++i)
	            {
	            	if(isdigit(cmd[cmd_cnt-1][i]) == 0)
	            		number_pipe = 0;
	            }
	        }
	        else number_pipe = 0;

	        if(number_pipe == 1 && strncmp(cmd[cmd_cnt-2],"!",1) == 0)
	        {
	        	number_pipe_type = 1;
	        }
	        

			int pfd[2];
			

			int fd_in = 0;
			int pre_fd_in = -1;
			int end_by_unknown = 0;
			int pipe_symbol = 0;
			int end_by_pipe = 0;
			
	        
			pid_t pid;	

			for(int i=0;i<pipe_number;++i)
			{
				char* exec_argv[cmd_cnt];
				int exec_argc = 0;
				int err_pfd[2];
				end_by_pipe = 0;
	            
				for(int j=pipe_symbol;j<cmd_cnt;++j)
				{

					if(strncmp(cmd[j],"|",1) == 0 || strncmp(cmd[j],"!",1) == 0)
					{
						exec_argv[exec_argc] = NULL;
						++exec_argc;
						pipe_symbol = j+1;
						end_by_pipe = 1;
						break;
					}
					else
					{
						exec_argv[exec_argc] = cmd[j];
						++exec_argc;
						if(j == cmd_cnt-1)
						{
							exec_argv[exec_argc] = NULL;
							++exec_argc;
						}
					}
				}
				

				if(check_exist(exec_argv[0]) == 1)
				{

					char exec_path[5000] = "/net/gcs/105/0556506/ras/bin/";
					strcat(exec_path,exec_argv[0]);
					
					

					if (pipe(pfd)<0)
					{
						perror("pipe failed");
						exit(1);
					}

					if(pipe(err_pfd) < 0)
					{
						perror("pipe failed");
						exit(1);
					}
	                
					pid = fork();

					if(pid<0) 
					{
						fprintf(stderr, "Fork Failed");
						exit(-1);

					}
					else if(pid==0) //child process
					{
						close(pfd[0]);
						close(err_pfd[0]);

						if(number_pipe_out_cnt > 0)
						{
							dup2(np_out[0],0);		
						} 
							
						else 
						{
							dup2(fd_in, 0);	
						}

						dup2(pfd[1], STDOUT_FILENO);
						dup2(err_pfd[1],STDERR_FILENO);
						
						
						execv(exec_path,exec_argv);
					}
					else
					{
						wait(NULL);

						bzero(buffer,15000);

						if(fd_in != 0)
							close(fd_in);
						
	                    if(number_pipe_type == 1)
	                    {
	                    	close(pfd[1]);

	                    	if(read(pfd[0],buffer,sizeof(buffer)) < 0)
								printf("Error read\n");

	                    	if(write(err_pfd[1],buffer,strlen(buffer)) < 0) 
	                    		error("ERROR writing to socket");

	                    	close(err_pfd[1]);
	                    	close(pfd[0]);

	                    	fd_in = err_pfd[0];
	                    }
	                   	else
	                   	{
	                   		close(err_pfd[1]);
	                   		close(pfd[1]);

	                   		int len = 0;
	                   		if(len = read(err_pfd[0],buffer,sizeof(buffer)) < 0)
								printf("Error read\n");

							if(strlen(buffer) > 0)
							{
	                   			if (write(newsockfd,buffer,strlen(buffer)) < 0) 
	                    			error("ERROR writing to socket");	    		
							}
	                   		close(err_pfd[0]);

	                    	fd_in = pfd[0];
	                   	}


	                   	if(i == pipe_number-1)
	                   	{
	                   		bzero(buffer,15000);
	                   		read(fd_in,buffer,sizeof(buffer));
	                   		close(fd_in);
	                   	}
						if(number_pipe_out_cnt > 0)
						{
							close(np_out[0]);
							number_pipe_out_cnt = 0;
						}
	                    
	                    
					}
				}
				else
				{
					if(number_pipe == 0)
					{
						end_by_unknown = 1;
						bzero(buffer,15000);
						strcpy(buffer,"Unknown command:[");
						strcat(buffer,exec_argv[0]);
						strcat(buffer,"].\n");
						n = write(newsockfd,buffer,strlen(buffer));

						if (n < 0) error("ERROR writing to socket");
					}
					else
					{

						number_pipe_fd[number_pipe_cnt] = fd_in;
						number_pipe_line[number_pipe_cnt] = atoi(exec_argv[0]);
						++number_pipe_cnt;
						number_pipe_type_array[number_pipe_cnt] = number_pipe_type;

					}
					
				}	
			}
			

			if(end_by_unknown != 1 && number_pipe == 0)
			{

	            if(write_file == 1)
	            {
	                FILE *f = fopen(file_name, "w");
	                if (f == NULL)
	                {
	                    printf("Error opening file!\n");
	                    exit(1);
	                }
	                //bzero(buffer,15000);
	                //int len = 0;
	                //if((len=read(fd_in, buffer, 15000))>0)
	                //{
	                    fprintf(f, buffer);
	                //}
	                fclose(f);
	                
	                
	            }
	            else
	            {
	                //bzero(buffer,15000);
	                //int len = 0;
	                //if((len=read(fd_in, buffer, 15000))>0)
	                //{
	                    n = write(newsockfd,buffer,strlen(buffer));
	             		
	                    if (n < 0) error("ERROR writing to socket");
	                //}
	                
	            }	
			}
		}
	}

	close(sockfd);
	return 0; 

}