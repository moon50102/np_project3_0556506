#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>

#include <unistd.h>

#include <string.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "Socket.c"

#define CLIENT_MAX_NUM 30
#define BUF_MAX_SIZE 15000
#define UNREAD_MSG_MAX_SIZE 29
struct client_info
{
	int id;
	int sock;
	char* name;
	char* ip_addr;
	int portno;
	int **number_pipes;
	int *number_pipe_counts;
	int pipe_max;
};
struct client_info clients[CLIENT_MAX_NUM];

struct unread_message
{
	int sender_id[UNREAD_MSG_MAX_SIZE];
	int pipe[UNREAD_MSG_MAX_SIZE][2];
};

struct unread_message namepipes[30];
int readline(int fd, char* ptr, int maxlen);
int splitLine(char* cmd,char** strArr);
void sendMessage(int socket, char* msg);
int commandHandler(struct client_info *client, char* cmdLine, char** cmdArr, int number);
int commandValidChecker(char* cmd, char* cmdpath, int client_id);

void broadcastMessage(char* message);
void removeClient(struct client_info c);
int addClient(int ssock, struct sockaddr_in cli_addr);
int clientIsExist(struct client_info c);

int namepipe_input_cmd_exist(char **cmdArr, int cmd_num);
int namepipe_exist( int sender_id, int receiver_id);
int namepipe_output_cmd_exist(char **cmdArr, int cmd_num);
int namepipe_index(int sender_id, int receiver_id);
void namepipeDestroy(int sender_id, int receiver_id);
void removeArrayElement(char **array, int size, int index);
int main( int argc, char *argv[] ){
	char buffer[BUF_MAX_SIZE];
	char hello_msg[] = "****************************************\n** Welcome to the information server. **\n****************************************\n";
	
	struct sockaddr_in cli_addr;
	int ssock, clilen;

	int msock;		/* master server socket */
	fd_set rfds;	/* read file descriptor set */
	fd_set afds;	/* active file descriptor set */
	int alen;		/* from-address length */
	int fd, nfds;

	/* initialize client table */
	for (int i = 0; i < CLIENT_MAX_NUM; ++i)
	{
		/* id == -1 means it is unused */
		clients[i].id = -1;
		clients[i].sock = -1;
		clients[i].name = NULL;
		clients[i].ip_addr = NULL;
		clients[i].portno = -1;
		clients[i].number_pipes = NULL;
		clients[i].number_pipe_counts = NULL;
		clients[i].pipe_max = 0;

		for (int j = 0; j < UNREAD_MSG_MAX_SIZE; ++j)
		{
			namepipes[i].sender_id[j] = -1;
			//printf("user id %d i sender_id %d is %d\n",i,j, namepipes[i].sender_id[j]);
		}
		
	}


	/* Change directory to home dir */
	chdir("/net/gcs/105/0556506/rwg/");
	/* First call to socket() function */
	msock = passiveSocket();

	/* Now start listening for the clients, here process will 
	* go in sleep mode and will wait for the incoming connection
	*/
	
	listen(msock, 30);
	nfds = getdtablesize();
	FD_ZERO(&afds);
	FD_SET(msock, &afds);
	while(1){
		fflush(stdout);
		memcpy(&rfds, &afds, sizeof(rfds));
		if(select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0){
			perror("select error");
		}

		if (FD_ISSET(msock, &rfds)) { 
			
			fflush(stdout);
			int ssock;
			clilen  = sizeof(cli_addr);
			ssock = accept(msock, (struct sockaddr *)&cli_addr, &clilen); 
			if(ssock < 0){
				perror("Server: Error on accept");
				exit(1);
			}

			printf("Welcome our new friend: %d!\n", ssock);			
			printf("client ip address: %s", inet_ntoa(cli_addr.sin_addr));
			printf("port is: %d\n", (int) ntohs(cli_addr.sin_port));
			
			//fprintf(buffer, "%s\n", );
			/* Print welcome message to client */
			write(ssock, hello_msg, strlen(hello_msg));
			FD_SET(ssock, &afds);

			/* setting client information*/
			int id = addClient(ssock, cli_addr);
			printf("client id = %d\n", id);

			bzero(buffer, BUF_MAX_SIZE);
			sprintf(buffer, "*** User '(no name)' entered from %s/%d. ***\n", "CGILAB", 511);
			broadcastMessage(buffer);
			write(ssock, "% ", 2);
		}

		for (fd=0; fd<nfds; ++fd){
			if (fd != msock && FD_ISSET(fd, &rfds)){
				/* Read command from client */
				bzero(buffer, BUF_MAX_SIZE);
				int n = readline(fd, buffer, 14999);
				if(n < 0){
					perror("Server: ERROR reading from socket");
					exit(1);
				}
				char* commands[BUF_MAX_SIZE];
				bzero(commands, BUF_MAX_SIZE);
				char command[BUF_MAX_SIZE];
				strcpy(command, buffer);
				printf("============================\nthe command is: %s\n==========================\n", buffer);
				int cmd_num = splitLine(buffer, commands);
				/* Disconnect if client send exit command*/
				int i;
				for (i = 0; i < CLIENT_MAX_NUM; ++i)
				{
					if ( clientIsExist(clients[i]) == 1 && clients[i].sock == fd)
					{
						break;	
					}
				}
				
				int state = commandHandler(&clients[i], command, commands, cmd_num);
				if (state == 0)
				{
					(void)close(fd);
					FD_CLR(fd, &afds);
					//break;
				}else{
					write(fd,  "% ", 2);
				}	
			}
		}

		//clilen  = sizeof(cli_addr);
		/* Accept actual connection from the client */
		// newsockfd = accept(msock, (struct sockaddr *)&cli_addr, &clilen);
		// if(newsockfd < 0){
		// 	perror("Server: Error on accept");
		// 	exit(1);
		// }

		// setenv("PATH", "bin:.", 1);
		//reset global variables
		// number_pipes = NULL;
		// number_pipe_counts = NULL;
		// pipe_max = 0;
		/* Print welcome message to client*/
		// int n = write(newsockfd, hello_msg, sizeof(hello_msg));
		// if(n < 0){
		// 	perror("Server: Error writing to socket");
		// 	exit(1);
		// }
		// n = write(newsockfd, "\n", 1);
		/* If connection is established then start communicating */
		// while(1){
		// 	/* Send prompt character */
		// 	n = write(newsockfd, "% ", 2);
		// 	if(n < 0){
		// 		perror("Server: Error writing to socket");
		// 		exit(1);
		// 	}

		// 	/* Read command from client */
		// 	bzero(buffer, 15000);
		// 	n = readline(newsockfd, buffer, 14999);
		// 	if(n < 0){
		// 		perror("Server: ERROR reading from socket");
		// 		exit(1);
		// 	}
		// 	printf("\n-------------------\nHere is the message: %s\n-------------------\n", buffer);
		// 	fflush(stdout);

		// 	char* commands[15000];
		// 	bzero(commands, 15000);
		// 	int cmd_num = splitLine(buffer, commands);
		// 	/* Disconnect if client send exit command*/
		// 	int state = commandHandler(newsockfd, commands, cmd_num);
		// 	if (state == 0)
		// 	{
		// 		close(newsockfd);
		// 		break;
		// 	}

		// }
		
	}

	return 0;
}

int readline(int fd, char* ptr, int maxlen){
	int n, rc;
	char c;
	*ptr = 0;
	for(n = 1; n < maxlen; n++){
		if((rc = read(fd, &c, 1)) == 1){
			if(c=='\n'){
				break;
			}
			if(c=='\r')
			{
				continue;
			}
			*ptr++ = c;
		}else if(rc == 0){
			if(n == 1)
				return 0;
			else
				break;
		}else{
			return -1;
		}
	}
	return n;
}

int splitLine(char* cmd,char** strArr){
	char *substr = strtok(cmd, " ");
	int index = 0;
	while(substr != NULL){
		strArr[index] = (char*)malloc((strlen(substr) + 1)*sizeof(char));
		strcpy(strArr[index], substr);
		index++;
		substr = strtok(NULL, " ");
	}
	return index;
}

void sendMessage(int socket, char* msg){

	int n = write(socket, msg, strlen(msg));
	if(n < 0){
		perror("Server: Error writing to socket");
		exit(1);
	}
}

int commandHandler(struct client_info *client, char* cmdLine, char** cmdArr, int number){

	char message[BUF_MAX_SIZE]; /* message send for client */
	int socket = client->sock;

	int npipe_n;
	int input_npipe_index = -1;

	for (npipe_n = 0; npipe_n < client->pipe_max; ++npipe_n)
	{
		client->number_pipe_counts[npipe_n]--;
		if (client->number_pipe_counts[npipe_n] == 0)
		{
			input_npipe_index = npipe_n;
		}
	}

	/* start handle commands */
	if (strncmp(cmdArr[0], "exit", strlen("exit")) == 0)
	{
		/* disconnect */
		bzero(message, sizeof(message));
		sprintf(message, "*** User '%s' left. ***", client->name);
		broadcastMessage(message);
		removeClient(*client);
		return 0;
	}else if(strncmp(cmdArr[0], "printenv", strlen("printenv")) == 0){
		
		if (number < 2){
			perror("Argument number invalid");
		}
		char env_name[100];
		sprintf(env_name, "%s_user%d", cmdArr[1], client->id);
		char* s = getenv(env_name);
		bzero(message, sizeof(message));
		strcpy(message,cmdArr[1]);
		strcat(message,"=");
		strcat(message,s);
		strcat(message,"\n");
		sendMessage(socket, message);

	}else if(strncmp(cmdArr[0], "setenv", strlen("setenv")) == 0){

		if (number < 3){
			perror("Argument number invalid");
		}
		char env_name[100];
		sprintf(env_name, "%s_user%d", cmdArr[1], client->id);
		printf("env_name: %s\n", env_name);
		setenv(env_name, cmdArr[2], 1);

	}else if(strncmp(cmdArr[0], "who", strlen("who")) == 0){

		char *format = "%d\t%s\t%s/%d\t";
		bzero(message, sizeof(message));
		sprintf(message, "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
		for (int i = 0; i < CLIENT_MAX_NUM; ++i)
		{
			
			if ( clientIsExist(clients[i]) == 1 )
			{
				char msg[BUF_MAX_SIZE];
				if (clients[i].id == client->id)
				{
					/* indicate me */
					sprintf(msg, format, clients[i].id, clients[i].name, clients[i].ip_addr, clients[i].portno);
					strcat(msg, "<-me\n");
				}else{

					sprintf(msg, format, clients[i].id, clients[i].name, clients[i].ip_addr, clients[i].portno);
					strcat(msg, "\n");
				}
				strcat(message, msg);
			}
		}
		sendMessage(socket, message);
	}else if(strncmp(cmdArr[0], "name", strlen("name")) == 0){
		
		for (int i = 0; i < CLIENT_MAX_NUM; ++i)
		{
			if (clientIsExist(clients[i]) && (strcmp(clients[i].name, cmdArr[1]) == 0) && (clients[i].id != client->id))
			{
				/* the nickname is alread used */
				bzero(message, sizeof(message));
				sprintf(message, "*** User '%s' already exists. ***\n", cmdArr[1]);
				sendMessage(socket, message);
				return 1;
			}
		}
		client->name = (char*)malloc(strlen(cmdArr[1])*sizeof(char));
		strcpy(client->name, cmdArr[1]);
		bzero(message, sizeof(message));
		sprintf(message, "*** User from %s/%d is named '%s'. ***\n", client->ip_addr, client->portno, client->name);
		broadcastMessage(message);

	}else if(strncmp(cmdArr[0], "yell", strlen("yell")) == 0){
		char command[BUF_MAX_SIZE];
		strcpy(command, cmdLine); 
		int command_line_len = strlen(command);
		int command_len = strlen(cmdArr[0]);

		char* content = malloc((command_line_len - command_len)*sizeof(char));

		for (int i = 0; i < command_line_len - command_len; ++i)
		{
			content[i] = command[i+command_len];
		}
		
		bzero(message, sizeof(message));
		sprintf(message, "*** %s yelled ***:%s\n", client->name, content);
		// for (int i = 1; i < number; ++i)
		// {
		// 	strcat(message, " ");
		// 	strcat(message, cmdArr[i]);
		// }
		// strcat(message, "\n");
		broadcastMessage(message);

	}else if(strncmp(cmdArr[0], "tell", strlen("tell")) == 0){
		char command[BUF_MAX_SIZE];
		strcpy(command, cmdLine); 
		int command_line_len = strlen(command);
		int command_len = strlen(cmdArr[0]) + strlen(cmdArr[1]) + 1;

		char* content = malloc((command_line_len - command_len)*sizeof(char));

		for (int i = 0; i < command_line_len - command_len; ++i)
		{
			content[i] = command[i+command_len];
		}

		for (int i = 0; i < CLIENT_MAX_NUM; ++i)
		{
			if (clients[i].id == atoi(cmdArr[1]))
			{
				int dest_sock = clients[i].sock;
				bzero(message, sizeof(message));
				sprintf(message, "*** %s told you ***:%s\n", client->name, content);
				// for (int i = 2; i < number; ++i)
				// {
				// 	strcat(message, " ");
				// 	strcat(message, cmdArr[i]);
				// }
				// strcat(message, "\n");
				sendMessage(dest_sock, message);
				return 1;
			}
		}
		/* client does not exist */
		bzero(message, sizeof(message));
		sprintf(message, "*** Error: user #%d does not exist yet. ***\n", atoi(cmdArr[1]));
		sendMessage(socket, message);
		
	}else{

		int input_namepipe_sender_id = -1 ;
		int namepipe_cmd_index = namepipe_input_cmd_exist(cmdArr, number);
		if (namepipe_cmd_index > -1)
		{
			printf("namepipe input command: %s", cmdArr[namepipe_cmd_index]);
			input_namepipe_sender_id = atoi(strtok(cmdArr[namepipe_cmd_index], "<"));
			printf("namepipe input sender id: %d", input_namepipe_sender_id);
			if ( namepipe_exist(input_namepipe_sender_id, client->id) == 0)
			{
				/* error: namepipe does not exist */
				bzero(message, sizeof(message));
				sprintf(message, "*** Error: the pipe #%d->#%d does not exist yet. *** \n", input_namepipe_sender_id, client->id);
				sendMessage(socket, message);
				return 1;
			}
			bzero(message, sizeof(message));
			sprintf(message, "*** %s (#%d) just received from %s (#%d) by '%s' ***\n", 	client->name, 
																						client->id, 
																						clients[input_namepipe_sender_id-1].name, 
																						input_namepipe_sender_id,
																						cmdLine);
			// for (int i = 0; i < number; ++i)
			// {
			// 	strcat(message, cmdArr[i]);
			// 	if (i != number-1)
			// 	{
			// 		strcat(message, " ");
			// 	}
				
			// }
			// strcat(message, "' ***\n");
			broadcastMessage(message);
			removeArrayElement(cmdArr, number, namepipe_cmd_index);
			number--;
		}

		int output_namepipe_receiver_id = -1 ;
		namepipe_cmd_index = namepipe_output_cmd_exist(cmdArr, number);
		if (namepipe_cmd_index > -1)
		{
			printf("namepipe input command: %s", cmdArr[namepipe_cmd_index]);
			output_namepipe_receiver_id = atoi(strtok(cmdArr[namepipe_cmd_index], ">"));
			printf("namepipe input receiver id: %d", output_namepipe_receiver_id);
			if ( clientIsExist(clients[output_namepipe_receiver_id-1]) == 0)
			{
				/* error: namepipe does not exist */
				bzero(message, sizeof(message));
				sprintf(message, "*** Error: user #%d does not exist yet. *** \n",output_namepipe_receiver_id);
				sendMessage(socket, message);
				return 1;
			}else if(namepipe_exist(client->id, output_namepipe_receiver_id)){
				/* error: namepipe does not exist */
				bzero(message, sizeof(message));
				sprintf(message, "*** Error: the pipe #%d->#%d already exists. *** \n",client->id, output_namepipe_receiver_id);
				sendMessage(socket, message);
				return 1;
			}
			bzero(message, sizeof(message));
			sprintf(message, "*** %s (#%d) just piped '%s' to %s (#%d) ***\n", 	client->name, 
																				client->id,
																				cmdLine,
																				clients[output_namepipe_receiver_id-1].name, 
																				output_namepipe_receiver_id
																				);
			// for (int i = 0; i < number; ++i)
			// {
			// 	strcat(message, cmdArr[i]);
			// 	if (i!=number-1)
			// 	{
			// 		strcat(message, " ");
			// 	}
			// }
			// char msg[100];
			// sprintf(msg, " to %s (#%d) ***\n", clients[output_namepipe_receiver_id-1].name, output_namepipe_receiver_id);
			// strcat(message, msg);
			broadcastMessage(message);
			removeArrayElement(cmdArr, number, namepipe_cmd_index);
			number--;
		}

		int cnt;
		int *pipe_index = NULL;
		int pipe_number = 0;
		for (cnt = 0; cnt < number; ++cnt)
		{
			if (strcmp(cmdArr[cnt], "|") == 0 || strcmp(cmdArr[cnt], ">") == 0 || strcmp(cmdArr[cnt], ">>") == 0)
			{
				pipe_number++;
				int *tmp = (int*) malloc(pipe_number*sizeof(int));
				if (pipe_index != NULL)
				{
					int j;
					for (j = 0; j < pipe_number-1; ++j)
					{
						tmp[j] = pipe_index[j];
					}
				}
				tmp[pipe_number-1] = cnt;
				free(pipe_index);
				pipe_index = tmp;
				tmp = NULL;
			}
		}

		char cmdpath[100];
		bzero(cmdpath, sizeof(cmdpath));
		char **argv;

		int i;
		int start_ind = 0;
		int end_ind = 0;
		int fdin = 0;
		for(i=0; i<=pipe_number; i++){

			if (i == pipe_number)
			{
				end_ind = number - 1;
				if (strncmp(cmdArr[number-1], "|", 1) == 0 || strncmp(cmdArr[number-1], "!", 1) == 0){
				
					end_ind --;
				}

			}else{

				end_ind = pipe_index[i] - 1;
			}

			if (commandValidChecker(cmdArr[start_ind], cmdpath, client->id) == 0)
			{
				/* command is unknown */
				bzero(message, sizeof(message));
				strcpy(message, "Unknown command: [");
				strcat(message, cmdArr[0]);
				strcat(message, "].\n");
				sendMessage(socket, message);
				break;

			}else{
				/* command is valid */

				/* read argument of command */
				int j;
				argv = NULL;
				argv = (char **)malloc((end_ind-start_ind+1+1)*sizeof(char*));
				for (j = 0; j <= end_ind-start_ind; ++j)
				{
					argv[j] = (char*)malloc(strlen(cmdArr[j+start_ind])*sizeof(char));
					strcpy(argv[j], cmdArr[j+start_ind]);
					printf("argv[%d]: %s\n", j, argv[j]);
				}
				argv[end_ind-start_ind+1] = NULL;
				//close(5);
				int pipe0[2];
				int pipe1[2];
				int pipe2[2];
				if(pipe(pipe0) < 0 || pipe(pipe1) < 0 || pipe(pipe2) < 0){
					perror("Can not create pipes");
				}

				if (i != 0)
				{
					fdin = pipe0[0];
					int n = write(pipe0[1], message, strlen(message));
					if (n < 0)
					{
						perror("Cannot write to pipe");
					}
				}
				int fdout = pipe1[1];
				int fderr = pipe2[1];
				
				if ((i < pipe_number) && (strcmp(cmdArr[pipe_index[i]], ">") == 0))
				{
					/* pipe to file */
					fdout = open(cmdArr[pipe_index[i]+1], O_RDWR|O_CREAT|O_TRUNC, 0666);
					printf("fdout: %d \n", fdout);
				}

				if ((i < pipe_number) && (strcmp(cmdArr[pipe_index[i]], ">>") == 0))
				{
					/* pipe to file */
					fdout = open(cmdArr[pipe_index[i]+1], O_RDWR|O_CREAT|O_APPEND, 0666);

				}
				
				if (input_npipe_index != -1 && client->number_pipes[input_npipe_index][1] != 0)
				{
					close(client->number_pipes[input_npipe_index][1]);
					client->number_pipes[input_npipe_index][1] = 0;
				}
				
				if(input_namepipe_sender_id != -1){
					int index = namepipe_index(input_namepipe_sender_id, client->id);
					close(namepipes[client->id-1].pipe[index][1]);
				}
				int pid = fork();
				if ( pid == 0 ){
					if ( i == 0 )
					{
						if (input_npipe_index != -1)
						{
							fdin = client->number_pipes[input_npipe_index][0];
							//int n = read(fdin, message, sizeof(message));
							//printf("number pipe input!!!!!!!!!!!n: %d\n", n);
						}else if (input_namepipe_sender_id != -1){

							int index = namepipe_index(input_namepipe_sender_id, client->id);
							fdin = namepipes[client->id-1].pipe[index][0];
						}
						close(pipe0[0]);
					}
					printf("child fdin is%d\n", fdin);
					printf("child fdout is%d\n", fdout);
					printf("child fderr is%d\n", fderr);
					close(pipe0[1]);
					close(pipe1[0]);
					close(pipe2[0]);
					
					dup2(fdin, STDIN_FILENO);
					dup2(fdout, STDOUT_FILENO);	
					dup2(fderr, STDERR_FILENO);
					execvp(cmdpath, argv);
					exit(0);
				}
				else if (pid > 0){
					/* parent process */

					close(pipe0[0]);
					close(pipe0[1]);
					close(pipe1[1]);
					close(pipe2[1]);
					wait(NULL);
					printf("pipe1[0]: %d\n",pipe1[0]);
					if (input_npipe_index != -1)
					{
						//printf("client->number_pipes[input_npipe_index][0] %d\n", client->number_pipes[input_npipe_index][0]);
						if (client->number_pipes[input_npipe_index][0] != 0)
						{
							close(client->number_pipes[input_npipe_index][0]);
							client->number_pipes[input_npipe_index][0] = 0;
						}
						
						//close(number_pipes[input_npipe_index][1]);
					}

					/* last one command*/
					if (i == pipe_number)
					{						
						if((end_ind != number -1)&&(strncmp(cmdArr[number-1], "|", 1) == 0)){
							/* number pipe */
							char *N_str = (char *)malloc(strlen(cmdArr[number-1])*sizeof(char));
							strcpy(N_str, cmdArr[number-1]);
							N_str++; /* eliminate the first character "|" */
							int N = atoi(N_str);
							// if N is in number_pipe_counts then add data to the pipe else create a new
							int k;
							int pipe_in_array = 0;
							for (k = 0; k < client->pipe_max; ++k)
							{
								if (N == client->number_pipe_counts[k])
								{
									/* add data into this pipe*/
									printf("in an exising pipe\n");
									char msg[BUF_MAX_SIZE];
									bzero(msg, sizeof(msg));
									int n = read(pipe1[0], msg, sizeof(msg));
									if(n >= 0){
										n = write(client->number_pipes[k][1], msg, strlen(msg));
										if (n < 0)
										{
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe1[0]);
									}
									pipe_in_array = 1;
									break;
								}
							}

							if (pipe_in_array == 0)
							{
								printf("not in an exising pipe\n");
								if (input_npipe_index != -1)
								{
									printf("just a pipe completed its mission, index is:%d\n", input_npipe_index);
									int tmp_pipe[2];
									pipe(tmp_pipe);
									client->number_pipes[input_npipe_index][0] = tmp_pipe[0];
									client->number_pipes[input_npipe_index][1] = tmp_pipe[1];

									client->number_pipe_counts[input_npipe_index] = N;

									char msg[BUF_MAX_SIZE];
									bzero(msg, sizeof(msg));
									int n = read(pipe1[0], msg, sizeof(msg));
									printf("********number pipe msg: %s\n" ,msg);
									if(n >= 0){
										n = write(client->number_pipes[input_npipe_index][1], msg, strlen(msg));
										if (n < 0)
										{
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe1[0]);
									}
								}else{

									printf("create a number pipe and now size is %d\n", client->pipe_max+1);
									/* create a pipe and add in array */
									/*
									int **tmp_pipes = (int **)malloc((client->pipe_max+1)*sizeof(int *));
									int *tmp_counts = (int *)malloc((client->pipe_max+1)*sizeof(int));
									for (k = 0; k < client->pipe_max; ++k)
									{
										tmp_pipes[k] = (int *)malloc(2*sizeof(int));
										int i;
										for (i = 0; i < 2; ++i)
										{
											tmp_pipes[k][i] = client->number_pipes[k][i];
										}
										tmp_counts[k] = client->number_pipe_counts[k];
									}

									int tmp_pipe[2];
									pipe(tmp_pipe);
									tmp_pipes[client->pipe_max] = (int *)malloc(2*sizeof(int));
									tmp_pipes[client->pipe_max] = tmp_pipe;
									
									free(client->number_pipes);
									client->number_pipes = tmp_pipes;
									tmp_pipes = NULL;

									
									

									char msg[BUF_MAX_SIZE];
									bzero(msg, sizeof(msg));
									int n = read(pipe1[0], msg, sizeof(msg));
									if(n >= 0){
										printf("********number pipe msg: %s\n" ,msg);
										n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
										if (n < 0)
										{
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe1[0]);
									}

									tmp_counts[client->pipe_max] = N;
									free(client->number_pipe_counts);
									client->number_pipe_counts = tmp_counts;
									tmp_counts = NULL;
									*/

									if (client->pipe_max == 0)
									{
										client->number_pipes = (int **)malloc((client->pipe_max+1)*sizeof(int*));
										for (int k = 0; k < client->pipe_max+1; ++k)
										{
											client->number_pipes[k] = (int *)malloc(2*sizeof(int));
										}
										client->number_pipe_counts = (int *)malloc((client->pipe_max+1)*sizeof(int));
									}else{

										client->number_pipes = (int **)realloc(client->number_pipes,(client->pipe_max+1)*sizeof(int));
										client->number_pipes[client->pipe_max] = (int *)malloc(2*sizeof(int));
										client->number_pipe_counts = (int *)realloc(client->number_pipe_counts,(client->pipe_max+1)*sizeof(int));
									}
									
									int tmp_pipe[2];
									pipe(tmp_pipe);
									client->number_pipes[client->pipe_max][0] = tmp_pipe[0];
									client->number_pipes[client->pipe_max][1] = tmp_pipe[1];
									char msg[BUF_MAX_SIZE];
									bzero(msg, sizeof(msg));
									int n = read(pipe1[0], msg, sizeof(msg));
									if(n >= 0){
										n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
										if (n < 0){
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe1[0]);
									}
									client->number_pipe_counts[client->pipe_max] = N;
									client->pipe_max++;
								}
							}

							bzero(message, sizeof(message));
							int n = read(pipe2[0], message, sizeof(message));
							if (n < 0){
								perror("Cannot read message from pipe");
								exit(1);
							}else if(n > 0){
								sendMessage(socket, message);	
							}
							
							close(pipe2[0]);

						}else if((end_ind != number -1)&&(strncmp(cmdArr[number-1], "!", 1) == 0)){

							/* erro message number pipe */
							char *N_str = (char *)malloc(strlen(cmdArr[number-1])*sizeof(char));
							strcpy(N_str, cmdArr[number-1]);
							N_str++; /* eliminate the first character "|" */
							int N = atoi(N_str);
							// if N is in number_pipe_counts then add data to the pipe else create a new
							int k;
							int pipe_in_array = 0;
							for (k = 0; k < client->pipe_max; ++k)
							{
								if (N == client->number_pipe_counts[k])
								{
									/* add data into this pipe*/
									char msg[BUF_MAX_SIZE];
									bzero(msg, sizeof(msg));
									int n = read(pipe2[0], msg, sizeof(msg));
									if(n >= 0){
										n = write(client->number_pipes[k][1], msg, strlen(msg));
										if (n < 0)
										{
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe2[0]);
									}

									n = read(pipe1[0], msg, sizeof(msg));
									if(n >= 0){
										n = write(client->number_pipes[k][1], msg, strlen(msg));
										if (n < 0)
										{
											perror("Cannot write message to pipe");
											exit(1);
										}
										close(pipe1[0]);
									}
									pipe_in_array = 1;
									break;
								}
							}

							if (pipe_in_array == 0)
							{
								/* create a pipe and add in array */
								// int **tmp_pipes = (int **)malloc((client->pipe_max+1)*sizeof(int *));
								// int *tmp_counts = (int *)malloc((client->pipe_max+1)*sizeof(int));
								// for (k = 0; k < client->pipe_max; ++k)
								// {
								// 	tmp_pipes[k] = (int *)malloc(2*sizeof(int));
								// 	int i;
								// 	for (i = 0; i < 2; ++i)
								// 	{
								// 		tmp_pipes[k][i] = client->number_pipes[k][i];
								// 	}
								// 	tmp_counts[k] = client->number_pipe_counts[k];
								// }

								// int tmp_pipe[2];
								// pipe(tmp_pipe);
								// tmp_pipes[client->pipe_max] = (int *)malloc(2*sizeof(int));
								// tmp_pipes[client->pipe_max] = tmp_pipe;
								
								// free(client->number_pipes);
								// client->number_pipes = tmp_pipes;
								// tmp_pipes = NULL;

								// char msg[BUF_MAX_SIZE];
								// bzero(msg, sizeof(msg));
								// int n = read(pipe2[0], msg, sizeof(msg));
								// if(n >= 0){
								// 	n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
								// 	if (n < 0)
								// 	{
								// 		perror("Cannot write message to pipe");
								// 		exit(1);
								// 	}
								// 	close(pipe2[0]);
								// }

								// n = read(pipe1[0], msg, sizeof(msg));
								// if(n >= 0){
								// 	n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
								// 	if (n < 0)
								// 	{
								// 		perror("Cannot write message to pipe");
								// 		exit(1);
								// 	}
								// 	close(pipe1[0]);
								// }

								// tmp_counts[client->pipe_max] = N;
								// free(client->number_pipe_counts);
								// client->number_pipe_counts = tmp_counts;
								// tmp_counts = NULL;

								// client->pipe_max++;

								if (client->pipe_max == 0)
								{
									client->number_pipes = (int **)malloc((client->pipe_max+1)*sizeof(int*));
									for (int k = 0; k < client->pipe_max+1; ++k)
									{
										client->number_pipes[k] = (int *)malloc(2*sizeof(int));
									}
									client->number_pipe_counts = (int *)malloc((client->pipe_max+1)*sizeof(int));
								}else{

									client->number_pipes = (int **)realloc(client->number_pipes,(client->pipe_max+1)*sizeof(int));
									client->number_pipes[client->pipe_max] = (int *)malloc(2*sizeof(int));
									client->number_pipe_counts = (int *)realloc(client->number_pipe_counts,(client->pipe_max+1)*sizeof(int));
								}
								
								int tmp_pipe[2];
								pipe(tmp_pipe);
								client->number_pipes[client->pipe_max][0] = tmp_pipe[0];
								client->number_pipes[client->pipe_max][1] = tmp_pipe[1];
								char msg[BUF_MAX_SIZE];
								bzero(msg, sizeof(msg));
								int n = read(pipe2[0], msg, sizeof(msg));
								if(n >= 0){
									n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
									if (n < 0)
									{
										perror("Cannot write message to pipe");
										exit(1);
									}
									close(pipe2[0]);
								}

								bzero(msg, sizeof(msg));
								n = read(pipe1[0], msg, sizeof(msg));
								if(n >= 0){
									n = write(client->number_pipes[client->pipe_max][1], msg, strlen(msg));
									if (n < 0){
										perror("Cannot write message to pipe");
										exit(1);
									}
									close(pipe1[0]);
								}
								client->number_pipe_counts[client->pipe_max] = N;
								client->pipe_max++;							
							}

						}else if(output_namepipe_receiver_id != -1){
							/* create a pipe and throw in output messages */
							printf("HELLLLLLLLLLLLLO %d\n", output_namepipe_receiver_id);
							int pipe_index = -1;
							for (int i = 0; i < UNREAD_MSG_MAX_SIZE; ++i)
							{
								printf("DE\t namepipes[%d].sender_id[%d] = %d\n", output_namepipe_receiver_id-1,i,namepipes[output_namepipe_receiver_id-1].sender_id[i]);
								fflush(stdout);
								if (namepipes[output_namepipe_receiver_id-1].sender_id[i] == -1)
								{
									namepipes[output_namepipe_receiver_id-1].sender_id[i] = client->id;
									pipe_index = i;
									break;
								}
							}

							if (pipe_index == -1)
							{
								printf("Unread message full !! \n");
								
							}else
							{
								printf("pipe index: %d\n", pipe_index);
							}

							int tmp_pipe[2];
							if(pipe(tmp_pipe) < 0){
								perror("Cannot create pipe");
								exit(1);
							}
							
							namepipes[output_namepipe_receiver_id-1].pipe[pipe_index][0] = tmp_pipe[0];
							namepipes[output_namepipe_receiver_id-1].pipe[pipe_index][1] = tmp_pipe[1];
							printf("namepipes[%d].pipe[%d][0] = %d\n", output_namepipe_receiver_id-1, pipe_index, tmp_pipe[0]);
							printf("namepipes[%d].pipe[%d][1] = %d\n", output_namepipe_receiver_id-1, pipe_index, tmp_pipe[1]);
							fflush(stdout);
							char msg[BUF_MAX_SIZE];
							bzero(msg, sizeof(msg));
							int n = read(pipe2[0], msg, sizeof(msg));
							if(n >= 0){
								printf("msg: %s\n", msg);
								n = write(namepipes[output_namepipe_receiver_id-1].pipe[pipe_index][1], msg, strlen(msg));
								if (n < 0)
								{
									perror("Cannot write message to pipe");
									exit(1);
								}
								close(pipe2[0]);
							}

							bzero(msg, sizeof(msg));
							n = read(pipe1[0], msg, sizeof(msg));
							if(n >= 0){
								printf("msg: %s\n", msg);
								n = write(namepipes[output_namepipe_receiver_id-1].pipe[pipe_index][1], msg, strlen(msg));
								if (n < 0){
									perror("Cannot write message to pipe");
									exit(1);
								}
								close(pipe1[0]);
							}

						
						}else{

							/* send message to client normally */
							bzero(message, sizeof(message));
							int n = read(pipe2[0], message, sizeof(message));
							if (n < 0){
								perror("Cannot read message from pipe");
								exit(1);
							}else if(n > 0){
								sendMessage(socket, message);	
							}
							close(pipe2[0]);

							bzero(message, sizeof(message));
							n = read(pipe1[0], message, sizeof(message));
							if (n < 0){
								perror("Cannot read message from pipe");
								exit(1);
							}else if(n > 0){
								sendMessage(socket, message);	
							}
							close(pipe1[0]);
						}
					}else{
						printf("not last one command\n");
						bzero(message, sizeof(message));
						int n = read(pipe2[0], message, sizeof(message));
						if (n < 0){
							
							perror("Cannot read error message from pipe");
							exit(1);
						}else if(n > 0){
							sendMessage(socket, message);	
						}
					
						close(pipe2[0]);

						bzero(message, sizeof(message));
						n = read(pipe1[0], message, sizeof(message));
						if (n < 0){
							perror("Cannot read stdout message from pipe");
							exit(1);
						}

						close(pipe1[0]);
					}

				}else{
					perror("error pid\n");
				}

				if ( i < pipe_number && strcmp(cmdArr[pipe_index[i]], ">") == 0)
				{
					break;
				}

				if ( i < pipe_number && strcmp(cmdArr[pipe_index[i]], ">>") == 0)
				{
					break;
				}
			}

			if (pipe_number > 0){

				start_ind = pipe_index[i] + 1;
			}

			if (input_namepipe_sender_id != -1)
			{
				namepipeDestroy(input_namepipe_sender_id, client->id);
			}
		}
			
	}
	return 1;
}

int commandValidChecker(char* cmd, char*cmdpath, int client_id){
	char home_dir[] = "/net/gcs/105/0556506/rwg/";
	DIR *dir;
	struct dirent *ent;
	char env_name[100];
	sprintf(env_name, "PATH_user%d", client_id);
	char* subdir = getenv(env_name);
	char* subpath = strtok(subdir, ":");
	while(subpath != NULL)
	{
		if (subpath[0] == '/')
		{
			subpath++;
		}
		char tmpdir[100];
		strcpy(tmpdir, home_dir);
		strcat(tmpdir, subpath);
		if ((dir = opendir (tmpdir)) != NULL) {
			/* print all the files and directories within directory */
			while ((ent = readdir (dir)) != NULL) {
				// files[i] = (char*)malloc((strlen(ent->d_name) + 1)*sizeof(char));
				if(strcmp(ent->d_name, cmd) == 0){
					strcpy(cmdpath, tmpdir);
					strcat(cmdpath, "/");
					strcat(cmdpath, ent->d_name);
					printf("cmdpath: %s\n",cmdpath);
					closedir (dir);
					return 1;
				}
		  	}
		  	closedir (dir);
		}else {
		  	/* could not open directory */
		  	perror ("cannot open directory");
		}
		subpath = strtok(NULL, ":");
	}
	return 0;
}

void broadcastMessage(char* message){
	
	char buffer[BUF_MAX_SIZE];	
	bzero(buffer, BUF_MAX_SIZE);
	strcpy(buffer, message);
	for(int i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if (clientIsExist(clients[i]))
		{
			int sock = clients[i].sock;
			write(sock, buffer, strlen(buffer));
		}	
	}
}

void removeClient(struct client_info c)
{
	for (int i = 0; i < CLIENT_MAX_NUM; ++i)
	{
		if (clients[i].id == c.id)
		{
			clients[i].id = -1;
			clients[i].sock = -1;
			clients[i].name = NULL;
			clients[i].ip_addr = NULL;
			clients[i].portno = -1;
			clients[i].number_pipes = NULL;
			clients[i].number_pipe_counts = NULL;
			clients[i].pipe_max = 0;
			break;
		}
	}

	for (int i = 0; i < UNREAD_MSG_MAX_SIZE; ++i)
	{
		if (namepipes[c.id-1].sender_id[i] != -1)
		{
			close(namepipes[c.id-1].pipe[i][0]);
			close(namepipes[c.id-1].pipe[i][1]);
			namepipes[c.id-1].sender_id[i] = -1;
		}
		
	}

	for (int i = 0; i < CLIENT_MAX_NUM; ++i)
	{

		for (int j = 0; j < UNREAD_MSG_MAX_SIZE; ++j)
		{
			if (namepipes[i].sender_id[j] == c.id)
			{
				close(namepipes[i].pipe[j][0]);
				close(namepipes[i].pipe[j][1]);
				namepipes[i].sender_id[j] = -1;
			}
		}
	}
}

int addClient(int ssock, struct sockaddr_in cli_addr){

	for (int i = 0; i < CLIENT_MAX_NUM; ++i)
	{
		/* find the smallest unused id */
		if (clients[i].id == -1)
		{
			int id = i+1;
			clients[i].id = id;
			clients[i].name = (char *)malloc(strlen("(no name)")*sizeof(char));
			strcpy(clients[i].name, "(no name)");
			clients[i].sock = ssock;
			// char* ip_addr = (char*)malloc(strlen(inet_ntoa(cli_addr.sin_addr))*sizeof(char));
			// strcpy(ip_addr, inet_ntoa(cli_addr.sin_addr));
			// clients[i].ip_addr = ip_addr;
			// clients[i].portno = (int) ntohs(cli_addr.sin_port);
			clients[i].ip_addr = (char*)malloc(strlen("CGILAB")*sizeof(char));;
			strcpy(clients[i].ip_addr, "CGILAB");
			clients[i].portno = 511;
			clients[i].pipe_max = 0;
			printf("%d %d %s %d\n", clients[i].id, clients[i].sock, clients[i].ip_addr, clients[i].portno);
			
			char env_name[100];
			sprintf(env_name, "PATH_user%d",id);
			setenv(env_name, "bin:.", 1);
			return id;
		}
	}
	return 0;
}

int clientIsExist(struct client_info c){
	
	if (c.id != -1)
	{
		return 1;
	}
	return 0;
}

int namepipe_input_cmd_exist(char **cmdArr, int cmd_num){

	for (int i = 0; i < cmd_num; ++i)
	{
		if( (strncmp(cmdArr[i], "<", 1) == 0 )&& (strlen(cmdArr[i]) > 1 )){
			return i;
		}	
	}
	return -1;
}

int namepipe_exist( int sender_id, int receiver_id){
	for (int i = 0; i < UNREAD_MSG_MAX_SIZE; ++i)
	{
		if(namepipes[receiver_id-1].sender_id[i] == sender_id)
			return 1;
	}
	return 0;
}

int namepipe_output_cmd_exist(char **cmdArr, int cmd_num){

	for (int i = 0; i < cmd_num; ++i)
	{
		if( (strncmp(cmdArr[i], ">", 1) == 0 )&& (strlen(cmdArr[i]) > 1 )){
			return i;
		}	
	}
	return -1;
}

int namepipe_index(int sender_id, int receiver_id){
	for (int i = 0; i < UNREAD_MSG_MAX_SIZE; ++i)
	{
		if(namepipes[receiver_id-1].sender_id[i] == sender_id)
			return i;
	}
	return -1;
}

void namepipeDestroy(int sender_id, int receiver_id){

	int index = namepipe_index(sender_id, receiver_id);
	if (index > -1)
	{
		namepipes[receiver_id-1].sender_id[index] = -1;
		namepipes[receiver_id-1].pipe[index][0] = 0;
		namepipes[receiver_id-1].pipe[index][1] = 0;
	}
}

void removeArrayElement(char **array, int size, int index){

	if (index >= size)
	{
		perror("out of index");
		exit(1);
	}
	for(int i = index; i < size; i++){
		array[i] = array[i+1];
	}
}