#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <sys/wait.h>
#include <ctype.h>
#include <arpa/inet.h>

#define CLIENT_CNT 30

char* WELCOME_MSG = "****************************************\n** Welcome to the information server. **\n****************************************\n";

char exec_list[80][255] = {};
char cmd[5000][1024];
int cmd_cnt = 0;
int client_cnt = 0;
int exec_list_cnt = 0;
int ipc_cnt = 0;
int ipc_src[100];
int ipc_dest[100];


struct clientInfo
{
	/* hw1 */
	char env_name[100][500];
	char env_argv[100][5000];
	int env_argc;

	int number_pipe_fd[3000];
	int number_pipe_line[3000];
	int number_pipe_cnt;
	int number_pipe_type_array[3000];
	
	/*hw2*/
	int fd;
	int pid;
	int id;
	int msg_cnt;
	int client_port;
	char client_ip[50];
	char client_name[20];
	char chat_buf[10][1024];

	
};

struct clientInfo clients[CLIENT_CNT];

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

void parse_cmd(char* cmd_input)
{
	
	cmd_cnt = 0;

	char cmd_line[15000];

	strcpy(cmd_line,cmd_input);
	for(int i=0;i<15000;++i)
	{
		if(cmd_line[i] == '\n' || cmd_line[i] == '\r')
			cmd_line[i] = ' ';
	}

	char* token = strtok(cmd_line," ");
	for(int i=0;i<2000;i++)
		strcpy(cmd[i],"");

	if(strcmp(token,"yell") == 0)
	{
		strcpy(cmd[0],token);
		int pos = 0;
		int msg_it = 0;
		char c;
		char msg[1024];
		bzero(msg,1024);
		for(int i=0;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] == 'y')
			{
				pos = i;
				break;
			}
		}

		for(int i=pos+4;i<strlen(cmd_input);++i)
		{
			if(isalpha(cmd_input[i]) != 0)
			{
				pos = i;
				break;
			}
		}

		for(int i=pos;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] != '\r' && cmd_input[i] != '\n')
			{
				msg[msg_it] = cmd_input[i];
				msg_it++;
			}	
		}

		strcpy(cmd[1],msg);
		cmd_cnt = 2;
		return;
	}
	else if(strcmp(token,"tell") == 0)
	{
		strcpy(cmd[0],token);
		token = strtok(NULL," ");
		strcpy(cmd[1],token);

		int pos = 0;
		int msg_it = 0;
		char c;
		char msg[1024];
		bzero(msg,1024);
		for(int i=0;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] == 't')
			{
				pos = i;
				break;
			}
		}

		for(int i=pos+4;i<strlen(cmd_input);++i)
		{
			if(isalpha(cmd_input[i]) != 0)
			{
				pos = i;
				break;
			}
		}

		for(int i=pos;i<strlen(cmd_input);++i)
		{
			if(cmd_input[i] != '\r' && cmd_input[i] != '\n')
			{
				msg[msg_it] = cmd_input[i];
				msg_it++;
			}	
		}

		strcpy(cmd[2],msg);
		cmd_cnt = 3;
		return;
	}

	while(token != NULL)
	{

		if(strcmp(token,"\n") == 0 || strcmp(token,"\r") == 0)
		{
			token = strtok(NULL," ");
			continue;
		}

		if(strncmp(token,"|",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],"|");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,"!",1) == 0 && strlen(token) > 1)
		{

			strcpy(cmd[cmd_cnt],"!");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,">",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],">");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else if(strncmp(token,"<",1) == 0 && strlen(token) > 1)
		{
			strcpy(cmd[cmd_cnt],"<");

			char temp[256] = "";
			for(int i=0;i<strlen(token)-1;i++)
				temp[i] = token[i+1];
			
			strcpy(cmd[cmd_cnt+1],temp);
			cmd_cnt = cmd_cnt + 2;
		}
		else
		{
			strcpy(cmd[cmd_cnt],token);
			cmd_cnt = cmd_cnt + 1;
		}
		
		token = strtok(NULL," ");
	}

}


void scan_bin(int it)
{
	DIR *dir;
	struct dirent *ent;
	char scan_path[10000] = "/net/gcs/105/0556506/rwg/";
	char temp[1000];
	strcpy(temp,clients[it].env_argv[0]);
	char* path_token = strtok(temp,":");
	exec_list_cnt = 0;

	while(path_token != NULL)
	{
		strcpy(scan_path,path_token);
		if ((dir = opendir (scan_path)) != NULL) 
		{
			/* print all the files and directories within directory */
			while ((ent = readdir (dir)) != NULL) 
			{
				if(strncmp(ent->d_name,".",1) != 0 && strncmp(ent->d_name,"..",2) != 0)
				{
					strcpy(exec_list[exec_list_cnt],ent->d_name);
					++exec_list_cnt;
				}
			}
			closedir(dir);
		} 
		else
		{
			/* could not open directory */
			perror ("");
		}
		path_token = strtok(NULL,":");
	}
	
}

int check_exist(char* check_cmd,int it)
{
	scan_bin(it);
	for(int i=0;i<exec_list_cnt;i++)
	{
		if(strcmp(check_cmd,exec_list[i]) == 0)
			return 1;
	}
	return 0;
}

void init_new_client(int fd,int port,char* ip)
{
	int empty = -1;
	for(int i=0;i<client_cnt;++i)
	{
		if(clients[i].fd == -1)
		{
			empty = i;
			break;
		}
	}

	if(empty == -1)
	{
		strcpy(clients[client_cnt].env_name[0],"PATH");
		strcpy(clients[client_cnt].env_argv[0],"bin:.");
		clients[client_cnt].env_argc = 1;
		clients[client_cnt].id = client_cnt + 1;
		clients[client_cnt].fd = fd;
		strcpy(clients[client_cnt].client_name,"(no name)");
		strcpy(clients[client_cnt].client_ip,ip);
		clients[client_cnt].client_port = port;
		client_cnt = client_cnt + 1;
	}
	else
	{
		strcpy(clients[empty].env_name[0],"PATH");
		strcpy(clients[empty].env_argv[0],"bin:."); 
		clients[empty].env_argc = 1;
		clients[empty].id = empty + 1;
		clients[empty].fd = fd;
		strcpy(clients[empty].client_name,"(no name)");
		strcpy(clients[empty].client_ip,ip);
		clients[empty].client_port = port;
	}
	
}

int find_by_fd(int fd)
{
	for(int i=0;i<client_cnt;++i)
	{
		if(clients[i].fd == fd)
			return i;
	}
}


void broadcast_msg(char* s)
{
	int n;
	for(int i=0;i<client_cnt;++i)
	{
		if(clients[i].fd == -1)
			continue;
		n = write(clients[i].fd,s,strlen(s));
		if(n < 0) perror("broadcast error");
	}
}

void clear_ipc_files(int id)
{
	char ipc_path[1000];
	
	for(int i=0;i<ipc_cnt;++i)
	{
		if(ipc_src[i] == id || ipc_dest[i] == id)
		{

			sprintf(ipc_path,"/net/gcs/105/0556506/tmp/w%dto%d",ipc_src[i],ipc_dest[i]);
			remove(ipc_path);
			ipc_src[i] = -1;
			ipc_dest[i] = -1;
		}
	}

	int temp = ipc_cnt-1;
	for(int i=0;i<temp;++i)
	{
		if(ipc_src[i] == -1 || ipc_dest[i] == -1)
		{
			ipc_src[i] = ipc_src[i+1] ;
			ipc_dest[i] = ipc_dest[i+1];
			ipc_cnt = ipc_cnt - 1;
		}	
	}
}


int main(int argc, char *argv[])
{
	chdir("/net/gcs/105/0556506/rwg");

	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[15000];
	char orig_cmd[15000];
	struct sockaddr_in serv_addr, cli_addr;
	fd_set rfds;
	fd_set afds;
	int n,nfds;

	if (argc < 2) {
	 fprintf(stderr,"ERROR, no port provided\n");
	 exit(1);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	int enable = 1;
	if (sockfd < 0 || setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int))<0) 
		error("ERROR opening socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
		error("ERROR on binding");
	listen(sockfd,5);
	

	nfds = getdtablesize();
	FD_ZERO(&afds);
	FD_SET(sockfd,&afds);

	while(1)
	{
		memcpy(&rfds,&afds,sizeof(rfds));

		if(select(nfds,&rfds,(fd_set*)0,(fd_set*)0,(struct timeval *)0) < 0) perror("select error");
		
		if(FD_ISSET(sockfd,&rfds))
		{
			clilen = sizeof(cli_addr);
			newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

			if (newsockfd < 0) error("ERROR on accept");

			printf("new socket accepted\n");

			init_new_client(newsockfd,(int) ntohs(cli_addr.sin_port),inet_ntoa(cli_addr.sin_addr));
			
		  	n = write(newsockfd,WELCOME_MSG,strlen(WELCOME_MSG));
			if (n < 0) error("ERROR writing to socket");

			bzero(buffer,15000);
			sprintf(buffer,"*** User '%s' entered from %s/%d. ***\n","(no name)",inet_ntoa(cli_addr.sin_addr),(int) ntohs(cli_addr.sin_port));
			broadcast_msg(buffer);

		  	FD_SET(newsockfd,&afds);

		  	n = write(newsockfd,"% ",2);
			if (n < 0) error("ERROR writing to socket");
		}
		
		for(int fd=1;fd<nfds;++fd)
		{
			if(fd != sockfd && FD_ISSET(fd,&rfds))
			{
				int it = find_by_fd(fd);
				int set_pipe_number = 0;
				
				bzero(buffer,15000);
				n = read(fd,buffer,15000);

				if (n < 0) error("ERROR reading from socket");

				strcpy(orig_cmd,buffer);
				printf("%s\n", orig_cmd);
				for(int i=0;i<strlen(orig_cmd);++i)
				{
					if( orig_cmd[i] == '\r' || orig_cmd[i] == '\n' )
						orig_cmd[i] = 0;
				}

				parse_cmd(buffer);

				if(strncmp(cmd[0],"exit",4) == 0)
				{
					set_pipe_number = 1;
					clients[it].number_pipe_cnt = 0;

					bzero(buffer,15000);

					sprintf(buffer,"*** User '%s' left. ***\n",clients[it].client_name);
					broadcast_msg(buffer);

					close(fd);
					FD_CLR(fd, &afds);
					clients[it].fd = -1;
					
					clear_ipc_files(clients[it].id);

				}
				else if(strncmp(cmd[0],"setenv",6) == 0)
				{
					set_pipe_number = 1;
					int env_exist = 0;
					for(int i=0;i<clients[it].env_argc;++i)
					{
						if(strcmp(clients[it].env_name[i],cmd[1]) == 0)
						{
							strcpy(clients[it].env_argv[i],cmd[2]);
							env_exist = 1;
							break;
						}
					}

					if(env_exist == 0)
					{
						strcpy(clients[it].env_name[clients[it].env_argc],cmd[1]);
						strcpy(clients[it].env_argv[clients[it].env_argc],cmd[2]);
						clients[it].env_argc++;
					}		
				}
				else if(strncmp(cmd[0],"printenv",8) == 0)
				{				
					set_pipe_number = 1;
					for(int i=0;i<clients[it].env_argc;++i)
					{
						if(strcmp(clients[it].env_name[i],cmd[1]) == 0)
						{
							char temp[10000];
							strcpy(temp,"");
							strcat(temp,clients[it].env_name[i]);
							strcat(temp,"=");
							strcat(temp,clients[it].env_argv[i]);
							strcat(temp,"\n");

							n = write(fd,temp,strlen(temp));
							if (n < 0) error("ERROR writing to socket");

							break;
						}
					}
				}
				else if(strncmp(cmd[0],"who",3) == 0)
				{
					set_pipe_number = 1;
					bzero(buffer,15000);
					strcpy(buffer,"<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
					char temp_who[1024];
					for(int i=0;i<client_cnt;++i)
					{
						if(clients[i].fd == -1)
							continue;
						if(clients[i].fd == fd)
							sprintf(temp_who,"%d\t%s\t%s/%d\t%s\n",clients[i].id,clients[i].client_name,clients[i].client_ip,clients[i].client_port,"<-me");
						else
							sprintf(temp_who,"%d\t%s\t%s/%d\t%s\n",clients[i].id,clients[i].client_name,clients[i].client_ip,clients[i].client_port,"");
						strcat(buffer,temp_who);
					}

					n = write(fd,buffer,strlen(buffer));
					if(n<0) perror("who: write error");
					
				}
				else if(strncmp(cmd[0],"name",4) == 0)
				{
					int name_used = 0;
					set_pipe_number = 1;

					for(int i=0;i<client_cnt;++i)
					{
						if(clients[i].fd == -1)
							continue;
						if(strcmp(clients[i].client_name,cmd[1]) == 0)
						{
							name_used = 1;
							break;
						}
					}

					bzero(buffer,15000);
					if(name_used == 1)
					{
						sprintf(buffer,"*** User '%s' already exists. ***\n",cmd[1]);
						n = write(fd,buffer,strlen(buffer));
						if(n<0) perror("name_used write error");
					}
					else
					{
						strcpy(clients[it].client_name,cmd[1]);
						sprintf(buffer,"*** User from %s/%d is named '%s'. ***\n",clients[it].client_ip,clients[it].client_port,cmd[1]);
						broadcast_msg(buffer);
					}
				}
				else if(strncmp(cmd[0],"yell",4) == 0)
				{
					set_pipe_number = 1;

					bzero(buffer,15000);
					sprintf(buffer,"*** %s yelled ***: %s\n",clients[it].client_name,cmd[1]);
					broadcast_msg(buffer);

				}
				else if(strncmp(cmd[0],"tell",4) == 0)
				{
					set_pipe_number = 1;
					int dest_it;
					int user_exist = 0;

					for(int i=0;i<client_cnt;++i)
					{
						if(clients[i].fd == -1)
							continue;
						if(clients[i].id == atoi(cmd[1]))
						{
							user_exist = 1;
							dest_it = i;
							break;
						}
					}

					bzero(buffer,15000);
					if(user_exist == 0)
					{
						sprintf(buffer,"*** Error: user #%s does not exist yet. ***\n",cmd[1]);
						n = write(fd,buffer,strlen(buffer));
						if(n<0) perror("user not found write error");
					}
					else
					{
						sprintf(buffer,"*** %s told you ***: %s\n",clients[it].client_name,cmd[2]);

						n = write(clients[dest_it].fd,buffer,strlen(buffer));
						if(n<0) perror("tell write error");
					}


				}
				
				
				int pipe_number = 1;
				

				for(int i=0;i<cmd_cnt;++i)
				{
					if(strncmp(cmd[i],"|",1) == 0 || strncmp(cmd[i],"!",1) == 0)
						++pipe_number;
				}
				
				

				int write_file = 0;
				int dest_exist = 0;
				int ipc_write = 0;
				int ipc_dest_id = 0;
				int ipc_wrtie_exist = 0;
				int write_file_pos = -1;
				char file_name[10000] ="";
				
				for(int i=0;i<cmd_cnt;++i)
				{
					if(strcmp(cmd[i],">") == 0)
					{
						write_file_pos = i;
						break;
					}
				}
				
				if(write_file_pos != -1)
				{
					
					for(int k=0;k<strlen(cmd[write_file_pos+1]);k++)
					{
						if(isdigit(cmd[write_file_pos+1][k]) == 0)
						{
							write_file = 1;
							break;
						}
					}
					
					if(write_file == 1)
					{
						strcpy(file_name,cmd[write_file_pos+1]);
						dest_exist = 1;	
					}
					else if(write_file == 0)
					{						
						ipc_write = 1;
						ipc_dest_id = atoi(cmd[write_file_pos+1]);

						for(int k=0;k<client_cnt;++k)
						{
							if(clients[k].fd == -1)
								continue;
							if(clients[k].id == (ipc_dest_id))
							{
								dest_exist = 1;
								break;
							}
						}

						if(dest_exist == 0)
						{
							bzero(buffer,15000);

							sprintf(buffer,"*** Error: user #%d does not exist yet. ***\n",ipc_dest_id);

							n = write(fd,buffer,strlen(buffer));
							if(n<0) perror("dest_exist == 0 error");
						}

						for(int k=0;k<ipc_cnt;k++)
						{
							if(ipc_dest[k] == ipc_dest_id && ipc_src[k] == clients[it].id)
							{
								ipc_wrtie_exist = 1;

								bzero(buffer,15000);
								sprintf(buffer,"*** Error: the pipe #%d->#%d already exists. ***\n",ipc_src[k],ipc_dest[k]);

								n = write(clients[it].fd,buffer,strlen(buffer));
							}
						}

						if(ipc_wrtie_exist == 0 && dest_exist == 1)
						{
							ipc_src[ipc_cnt] = clients[it].id;
							ipc_dest[ipc_cnt] = ipc_dest_id;
							ipc_cnt = ipc_cnt + 1;

							bzero(buffer,15000);

							sprintf(buffer,"*** %s (#%d) just piped '%s' to %s (#%d) ***\n",clients[it].client_name,clients[it].id,orig_cmd,clients[ipc_dest_id-1].client_name,ipc_dest_id);
							
							broadcast_msg(buffer);
						}
					}
					
					if(write_file_pos < (cmd_cnt-2))
					{
						strcpy(cmd[write_file_pos],cmd[write_file_pos+2]);
						strcpy(cmd[write_file_pos+1],cmd[write_file_pos+3]);
						for(int i=write_file_pos+2;i<cmd_cnt-1;++i)
						{
							strcpy(cmd[i],cmd[i+1]);
						}
					}		
					cmd_cnt = cmd_cnt-2;
				}	
				else dest_exist = 1;
				
				if(ipc_wrtie_exist == 1 || dest_exist == 0)
					set_pipe_number = 1;
				

				int ipc_read = 0;
				int ipc_src_id = 0;
				int ipc_read_exist = 0;
				int ipc_pipe[2];
				int read_pos = -1;
				
				for(int i=0;i<cmd_cnt;++i)
				{
					if(strncmp(cmd[i],"<",1) == 0)
					{
						read_pos = i;
						break;
					}
				}

				if(read_pos != -1)
				{
					ipc_read = 1;
					ipc_src_id = atoi(cmd[read_pos+1]);
					

					for(int i=0;i<ipc_cnt;++i)
					{
						if(ipc_src_id == ipc_src[i] && ipc_dest[i] == clients[it].id)
						{
							ipc_read_exist = 1;
							for(int k=i;k<(ipc_cnt-1);++k)
							{
								ipc_src[k] = ipc_src[k+1];
								ipc_dest[k] = ipc_dest[k+1];
							}

							ipc_cnt = ipc_cnt - 1;
							break;
						}
					}
					
					if(ipc_read_exist == 0)
					{
						bzero(buffer,15000);
						sprintf(buffer,"*** Error: the pipe #%d->#%d does not exist yet. ***\n",ipc_src_id,clients[it].id);

						n = write(clients[it].fd,buffer,strlen(buffer));
						if(n<0) perror("ipc_read_exist error");

						set_pipe_number = 1;
					}
					else
					{
						char broadcast_buf[15000];
						bzero(broadcast_buf,15000);
						int find_index = 0;
						for(int i=0;i<client_cnt;++i)
						{
							if(clients[i].fd == -1)
								continue;
							if( (ipc_src_id-1) == clients[i].id )
							{
								find_index = i;
								break;
							}
						}

						sprintf(broadcast_buf,"*** %s (#%d) just received from %s (#%d) by '%s' ***\n",clients[it].client_name,clients[it].id,clients[find_index].client_name,ipc_src_id,orig_cmd);
						
						broadcast_msg(broadcast_buf);
						
						FILE* f;
						char ipc_read_path[100];

						sprintf(ipc_read_path,"/net/gcs/105/0556506/tmp/w%dto%d",ipc_src_id,clients[it].id);
						

						f = fopen(ipc_read_path,"r");

						fseek(f, 0, SEEK_END);
						long fsize = ftell(f);
						fseek(f, 0, SEEK_SET);  

						char *str = malloc(fsize + 1);
						fread(str, fsize, 1, f);

						pipe(ipc_pipe);
						

						if(write(ipc_pipe[1],str,strlen(str)) < 0)
							perror("ipc Pipe write error\n");
						
						fclose(f);
						remove(ipc_read_path);
					 	close(ipc_pipe[1]);
					}
					if(read_pos < (cmd_cnt-2))
					{
						strcpy(cmd[read_pos],cmd[read_pos+2]);
						strcpy(cmd[read_pos+1],cmd[read_pos+3]);
						for(int i=read_pos+2;i<cmd_cnt-1;++i)
						{
							strcpy(cmd[i],cmd[i+1]);
						}
					}		
					cmd_cnt = cmd_cnt-2;
				}


			 
			 	for(int i=0;i<clients[it].number_pipe_cnt;++i)
			 	{
		  			if(clients[it].number_pipe_line[i] > -1)
			 			clients[it].number_pipe_line[i] =  clients[it].number_pipe_line[i] - 1;
			 	}
				


				int number_pipe_out_cnt = 0;
				int np_out[2];
				pipe(np_out);
				
				if(clients[it].number_pipe_cnt > 0)
				{
					for(int i=0;i<clients[it].number_pipe_cnt;++i)
					{

						if(clients[it].number_pipe_line[i] == 0)
						{
							bzero(buffer,15000);
							if(read(clients[it].number_pipe_fd[i],buffer,sizeof(buffer)) < 0)
								printf("Number Pipe read error\n");

							if(write(np_out[1],buffer,strlen(buffer)) < 0)
								printf("Number Pipe write error\n");

							

							close(clients[it].number_pipe_fd[i]);
							number_pipe_out_cnt++;
						}
					}
				}
				if(number_pipe_out_cnt == 0)
				{
					close(np_out[0]);
				}
			 	
			 	close(np_out[1]);
				
				int number_pipe = 1;
				int number_pipe_type = 0;

				
				if(strncmp(cmd[cmd_cnt-2],"|",1) == 0 || strncmp(cmd[cmd_cnt-2],"!",1) == 0)
				{

					for(int i=0;i<strlen(cmd[cmd_cnt-1]);++i)
					{
						if(isdigit(cmd[cmd_cnt-1][i]) == 0)
							number_pipe = 0;
					}
				}
				else number_pipe = 0;

				if(number_pipe == 1 && strncmp(cmd[cmd_cnt-2],"!",1) == 0)
				{
					number_pipe_type = 1;
				}
				
				if(set_pipe_number == 1)
					pipe_number = 0;
				int pfd[2];
				

				int fd_in = 0;
				int pre_fd_in = -1;
				int end_by_unknown = 0;
				int pipe_symbol = 0;
				int end_by_pipe = 0;
				
				
				pid_t pid;	

				for(int i=0;i<pipe_number;++i)
				{
					char* exec_argv[cmd_cnt];
					int exec_argc = 0;
					int err_pfd[2];
					end_by_pipe = 0;
					
					for(int j=pipe_symbol;j<cmd_cnt;++j)
					{

						if(strncmp(cmd[j],"|",1) == 0 || strncmp(cmd[j],"!",1) == 0)
						{
							exec_argv[exec_argc] = NULL;
							++exec_argc;
							pipe_symbol = j+1;
							end_by_pipe = 1;
							break;
						}
						else
						{
							exec_argv[exec_argc] = cmd[j];
							++exec_argc;
							if(j == cmd_cnt-1)
							{
								exec_argv[exec_argc] = NULL;
								++exec_argc;
							}
						}
					}
					

					if(check_exist(exec_argv[0],it) == 1)
					{

						char exec_path[5000] = "/net/gcs/105/0556506/rwg/bin/";
						strcat(exec_path,exec_argv[0]);
						
						if (pipe(pfd)<0)
						{
							perror("pipe failed");
							exit(1);
						}

						if(pipe(err_pfd) < 0)
						{
							perror("pipe failed");
							exit(1);
						}
						
						pid = fork();

						if(pid<0) 
						{
							fprintf(stderr, "Fork Failed");
							exit(-1);

						}
						else if(pid==0) //child process
						{
							close(pfd[0]);
							close(err_pfd[0]);

							if(number_pipe_out_cnt > 0)
							{
								dup2(np_out[0],0);		
							} 
								
							else 
							{
								dup2(fd_in, 0);	
							}

							if(ipc_read_exist == 1)
							{
								dup2(ipc_pipe[0],0);
							}

							dup2(pfd[1], STDOUT_FILENO);
							dup2(err_pfd[1],STDERR_FILENO);
							
							
							execv(exec_path,exec_argv);
						}
						else
						{
							wait(NULL);

							bzero(buffer,15000);

							if(fd_in != 0)
								close(fd_in);
							
							if(number_pipe_type == 1)
							{
								close(pfd[1]);

								if(read(pfd[0],buffer,sizeof(buffer)) < 0)
									printf("Error read\n");

								if(write(err_pfd[1],buffer,strlen(buffer)) < 0) 
									error("ERROR writing to socket");

								close(err_pfd[1]);
								close(pfd[0]);

								fd_in = err_pfd[0];
							}
						   	else
						   	{
						   		close(err_pfd[1]);
						   		close(pfd[1]);

						   		int len = 0;
						   		if(len = read(err_pfd[0],buffer,sizeof(buffer)) < 0)
									printf("Error read\n");

								if(strlen(buffer) > 0)
								{
						   			if (write(fd,buffer,strlen(buffer)) < 0) 
										error("ERROR writing to socket");				
								}
						   		close(err_pfd[0]);

								fd_in = pfd[0];
						   	}


						   	if(i == pipe_number-1)
						   	{
						   		bzero(buffer,15000);
						   		read(fd_in,buffer,sizeof(buffer));
						   		close(fd_in);
						   	}
							if(number_pipe_out_cnt > 0)
							{
								close(np_out[0]);
								number_pipe_out_cnt = 0;
							}
							
							
						}
					}
					else
					{
						if(number_pipe == 0)
						{
							end_by_unknown = 1;
							bzero(buffer,15000);
							strcpy(buffer,"Unknown command:[");
							strcat(buffer,exec_argv[0]);
							strcat(buffer,"].\n");
							n = write(fd,buffer,strlen(buffer));

							if (n < 0) error("ERROR writing to socket");
							break;
						}
						else
						{
							
							clients[it].number_pipe_fd[clients[it].number_pipe_cnt] = fd_in;
							clients[it].number_pipe_line[clients[it].number_pipe_cnt] = atoi(exec_argv[0]);
							++clients[it].number_pipe_cnt;
							clients[it].number_pipe_type_array[clients[it].number_pipe_cnt] = number_pipe_type;
							
						}
						
					}	
				}

				if(end_by_unknown != 1 && number_pipe == 0 && set_pipe_number == 0)
				{

					if(write_file == 1)
					{
						FILE *f = fopen(file_name, "w");
						if (f == NULL)
						{
							printf("Error opening file!\n");
							exit(1);
						}
						
						fprintf(f, buffer);
						
						fclose(f);
						
					}
					else if(ipc_write == 1)
					{

						FILE* f;
						char ipc_write_path[100];

						sprintf(ipc_write_path,"/net/gcs/105/0556506/tmp/w%dto%d",clients[it].id,ipc_dest_id);

						f = fopen(ipc_write_path,"w");
						
						fprintf(f,buffer);

						fclose(f);
					}
					else
					{
						n = write(fd,buffer,strlen(buffer));
				 		
						if (n < 0) error("ERROR writing to socket");
					}	
				}
					
				if(strcmp(cmd[0],"exit") != 0)
				{
					n = write(fd,"% ",2);
					if (n < 0) error("ERROR writing to socket");
				}
				

			}
		}
		
	}

	close(sockfd);
	return 0; 

}