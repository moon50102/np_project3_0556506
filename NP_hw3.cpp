#include <windows.h>
#include <iostream>
#include <list>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <io.h>

using namespace std;

#include "resource.h"

#define SERVER_PORT 7799
#define MAXUSER 5
#define WM_SOCKET_NOTIFY (WM_USER + 1)
#define EXIT_STR "exit\r\n"

using namespace std;

char h[MAXUSER][100];
char p[MAXUSER][100];
char f[MAXUSER][100];
char debug_msg[3000];
SOCKET client_fd[MAXUSER];
FILE *fd[MAXUSER];
int user_count = 0;
int write_enable[MAXUSER];
int unsend[MAXUSER];
SOCKET browser_sock;

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);
void print_default_html(int c,SOCKET s);
int contain_prompt ( char* line );
void print_replace(char* s,SOCKET sock)
{
	for(int i=0;i<strlen(s);++i)
	{
		char c[1];
		c[0] = s[i];
		if(s[i] == '\"') send(sock,"&quot;",6,0);
		else if(s[i] == '\'') send(sock,"&apos;",6,0);
		else if(s[i] == '&') send(sock,"&amp;",4,0);
		else if(s[i] == '<') send(sock,"&lt;",4,0);
		else if(s[i] == '>') send(sock,"&gt;",4,0);
		else if(s[i] == '\n') send(sock,"<br>",4,0);
		else send(sock,c,1,0);
	}
}
void print_html(char* s,int pos,int is_cmd,SOCKET sock)
{
	char temp[3000];
	char format_s[3000];

	strcpy(temp,s);
	for(int i=0;i<strlen(temp);++i)
	{
		if(temp[i] == '\r')
			temp[i] = ' ';
	}

	if(is_cmd == 1)
	{
		sprintf(format_s,"<script>document.all['m%d'].innerHTML += \" <b>",pos);
		send(sock,format_s,strlen(format_s),0);
		print_replace(temp,sock);
		sprintf(format_s,"</b><br>\";</script>\r\n");
		send(sock,format_s,strlen(format_s),0);
	}
	else if(contain_prompt(s))
	{
		sprintf(format_s,"<script>document.all['m%d'].innerHTML += \" %s\";</script>\r\n",pos,temp);
		send(sock,format_s,strlen(format_s),0);
	}
	else
	{
		sprintf(format_s,"<script>document.all['m%d'].innerHTML += \" ",pos);
		send(browser_sock,format_s,strlen(format_s),0);
		print_replace(temp,browser_sock);
		sprintf(format_s,"<br>\";</script>\r\n");
		send(browser_sock,format_s,strlen(format_s),0);
	}
		
}
int contain_prompt ( char* line )
{
	int i, prompt = 0 ;
	for (i=0; line[i]; ++i) {
		switch ( line[i] ) {
			case '%' : prompt = 1 ; break;
			case ' ' : if ( prompt ) return 1;
			default: prompt = 0;
		}
	}
	return 0;
} 

int recv_msg(int userno,SOCKET from)
{
	char buf[3000],*tmp;
	int len,i;
	
	len=recv(from,buf,sizeof(buf)-1,0);
	if(len < 0) return -1;
	strncpy(debug_msg,buf,50);
	buf[len] = 0;
	if(len>0)
	{
		for(tmp=strtok(buf,"\n"); tmp; tmp=strtok(NULL,"\n"))
		{
			if ( contain_prompt(tmp) ) write_enable[userno] = 1 ;
			print_html(tmp,userno,0,browser_sock);
		}
	}
	return len;
}

int readline(int fd,char *ptr,int maxlen)
{
	int n, rc;
	char c;
	*ptr = 0;
	for(n=1; n<maxlen; n++)
	{
		rc=_read(fd,&c,1);
		if(rc== 1)
		{
			*ptr++ = c;
			if(c=='\n')	break;
		}
		else if(rc==0)
		{
			if(n==1)		 return 0;
			else				 break;
		}
		else return(-1);
	}
	return n;
}
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;
	
	
	int err;


	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}
					
					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
					
					char mesg[99999], *reqline[3], data_to_send[1024];
					int rcvd, file_fd, bytes_read;
		
					memset( (void*)mesg, (int)'\0', 99999 );

					rcvd=recv(ssock, mesg, 99999, 0);

					if (rcvd<0)    // receive error
						fprintf(stderr,("recv() error\n"));
					else if (rcvd==0)    // receive socket closed
						fprintf(stderr,"Client disconnected upexpectedly.\n");
					else    // message received
					{
						
						reqline[0] = strtok (mesg, " \t\n");
						
						if ( strncmp(reqline[0], "GET\0", 4)==0 )
						{
							reqline[1] = strtok (NULL, " \t");
							reqline[2] = strtok (NULL, " \t\n");
							
							if ( strncmp( reqline[2], "HTTP/1.0", 8)!=0 && strncmp( reqline[2], "HTTP/1.1", 8)!=0 )
							{
								send(ssock, "HTTP/1.0 400 Bad Request\n", 25,0);
							}
							else
							{
								EditPrintf(hwndEdit, TEXT("MSG:%s\r\n"), reqline[1]);
								if(strncmp(reqline[1],"/form_get.htm",13) == 0)
								{
									browser_sock = ssock;
									if ( (file_fd=_open("form_get.htm", O_RDONLY))!=-1 )    //FILE FOUND
									{
										send(ssock, "HTTP/1.0 200 OK\n\n", 17, 0);
										while ( (bytes_read=_read(file_fd, data_to_send, 1024))>0 )
										{
											send (ssock, data_to_send, bytes_read,0);
											
											//printf("%s\n", data_to_send);
										}
										EditPrintf(hwndEdit, TEXT("MSG:%s\r\n"), "SEND FINISH");
									}
									
									err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY,FD_ACCEPT);
								}
								else if(strncmp(reqline[1],"/hw3.cgi",8) == 0)
								{
									browser_sock = ssock;
									
									char* s1 = strtok(reqline[1],"?");
									char* s2 = strtok(NULL," ");

									char temp[1000];
									strcpy(temp,s2);
									for(int i=0;i<strlen(temp);++i)
									{
										if(temp[i] == '&')
											temp[i] = ' ';
									}
									sscanf(temp,"h1=%s p1=%s f1=%s h2=%s p2=%s f2=%s h3=%s p3=%s f3=%s h4=%s p4=%s f4=%s h5=%s p5=%s f5=%s",&h[0],&p[0],&f[0],&h[1],&p[1],&f[1],&h[2],&p[2],&f[2],&h[3],&p[3],&f[3],&h[4],&p[4],&f[4]);


									struct hostent *he;
									int iResult;
									int sv_port;
									struct sockaddr_in client_sin[MAXUSER];

									for(int i=0;i<MAXUSER;++i) write_enable[i] = 0;
									for(int i=0;i<MAXUSER;++i) unsend[i] = 0;

									user_count = 0;
									for(int i=0;i<MAXUSER;++i)
									{

										if(strlen(h[i]) > 3)
										{
											user_count++;
										}
									}
									for(int i=0;i<user_count;++i)
									{
										
										fd[i] = fopen(f[i], "r");

										he = gethostbyname(h[i]);
										sv_port = atoi(p[i]);

										memset(&client_sin[i], 0, sizeof(client_sin[i])); 

										client_sin[i].sin_family = AF_INET;
										client_sin[i].sin_addr = *((struct in_addr *)he->h_addr);
										client_sin[i].sin_port = htons(sv_port);

										
									}
									
									for(int i=0;i<user_count;++i)
									{
										client_fd[i] = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
										connect(client_fd[i],(struct sockaddr *)&client_sin[i], sizeof(client_sin[i]));
										err = WSAAsyncSelect(client_fd[i], hwnd, WM_SOCKET_NOTIFY,FD_READ);
										if(err < 0){
											EditPrintf(hwndEdit, TEXT("SELECT error"));
										}
									}

									
									send(ssock, "HTTP/1.0 200 OK\n", 17, 0);

									print_default_html(user_count,ssock);

									err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY,FD_READ);
								}
								else err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY,FD_ACCEPT);
							}
								
						}
					}

					
					break;
				case FD_READ:
				//Write your code for read event here.
					EditPrintf(hwndEdit, TEXT("=== Reading ===\r\n"));
					for(int i=0;i<user_count;++i)
					{
						if(recv_msg(i, client_fd[i]) < 0)
						{
							//close(client_fd[i]);
							EditPrintf(hwndEdit, TEXT("=== Reading FAIL===\r\n"));
						}
						EditPrintf(hwndEdit, TEXT("READ MSG:%s\r\n"), debug_msg);
						if ( write_enable[i] )
						{
							err = WSAAsyncSelect(browser_sock, hwnd, WM_SOCKET_NOTIFY,FD_WRITE);
						}
						//else err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY,FD_READ);
					}
					break;
				case FD_WRITE:
				//Write your code for write event here
					char msg_buf[5000];
					EditPrintf(hwndEdit, TEXT("=== Writing ===\r\n"));

					for(int i=0;i<user_count;++i)
					{
						if ( write_enable[i] == 0)
						{
							err = WSAAsyncSelect(browser_sock, hwnd, WM_SOCKET_NOTIFY,FD_READ);
							continue;
						}
						if(!unsend[i]) {
							//send meesage
							 
							memset(msg_buf,0,sizeof(msg_buf));
							int len = readline(fileno(fd[i]), msg_buf, sizeof(msg_buf));
							EditPrintf(hwndEdit, TEXT("WRITE MSG:%s\r\n"), msg_buf);
							if(len < 0) exit(1);
							msg_buf[len] = 0;
							fflush(stdout);
						}

						unsend[i] = 0;
						
							if ( write_enable[i] ) {
								print_html(msg_buf,i,1,browser_sock);
								if(i<user_count){								
									send(client_fd[i], msg_buf, strlen(msg_buf),0);
									write_enable[i] = 0; 
									
									err = WSAAsyncSelect(browser_sock, hwnd, WM_SOCKET_NOTIFY,FD_READ);
									
								}
							} else {
								unsend[i] = 1 ;
							}
						
					}
					
					break;
				case FD_CLOSE:
					break;
			};
			break;
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}

void print_default_html(int c,SOCKET s)
{
	char tmp[5000] = "Content-Type: text/html\r\n\r\n<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\"/>\r\n<title>Network Programming Homework 3</title>\r\n</head>\n<body bgcolor=#336699>\r\n<font face=\"Courier New\" size=2 color=#FFFF99>\r\n<table width=\"800\" border=\"1\">\r\n<tr>\n";
	for(int i=0;i<c;++i)
	{
		strcat(tmp,"<td>");
		strcat(tmp,h[i]);
		strcat(tmp,"</td>");
	}
	strcat(tmp,"</tr>\r\n<tr>\r\n");

	for(int i=0;i<c;++i)
	{
		strcat(tmp,"<td valign=\"top\" id=\"m");
		tmp[strlen(tmp)] = i + '0';
		strcat(tmp,"\"></td>");
	}
	strcat(tmp,"</tr>\r\n</table>\r\n</font>\r\n</body>\r\n</html>\r\n");

	send(s,tmp,strlen(tmp),0);
}